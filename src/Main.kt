fun main(args: Array<String>)
{
    val bike = Bike()
    val basicBike = bike.clone()
    val advancedBike = makeJaguar(basicBike)
    println("Prototype Design Pattern: " + advancedBike.model!!)
}
